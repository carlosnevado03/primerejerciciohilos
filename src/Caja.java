import java.util.concurrent.Semaphore;

public class Caja implements Runnable {
    private String nombre;
    private int numProductos;
    private Semaphore semaphore;

    public Caja(String nombre, int numProductos, Semaphore semaphore) {
        this.nombre = nombre;
        this.numProductos = numProductos;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            // Esperar a adquirir un permiso del semáforo
            semaphore.acquire();

            System.out.println(nombre + " está abierta y lista para recibir clientes.");

            // Simulamos el procesamiento de los productos
            for (int i = 1; i <= numProductos; i++) {
                // Imprimir mensaje indicando el cliente y el producto que se está procesando
                System.out.println(nombre + " - Cliente: Cliente " + i + " - Producto: Producto " + i);
                // Simular el tiempo de procesamiento de cada producto con un tiempo aleatorio
                Thread.sleep((long) (Math.random() * 1000)); // Tiempo aleatorio para procesar cada producto
            }

            // Imprimir mensaje indicando que la caja ha terminado de atender a todos los clientes
            System.out.println(nombre + " ha terminado de atender a todos los clientes.");

        } catch (InterruptedException e) {
            // Manejar cualquier excepción de interrupción de hilos
            System.out.println("Error");;
        } finally {
            // Liberar el permiso del semáforo después de su uso
            semaphore.release();
        }
    }
}

