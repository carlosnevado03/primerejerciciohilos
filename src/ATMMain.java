import java.util.concurrent.Semaphore;

public class ATMMain {
    public static void main(String[] args) {
        // Creamos un semáforo con 3 permisos disponibles
        Semaphore semaphore = new Semaphore(3);

        // Lanzamos 5 cajeros con distintos nombres a la vez
        ATM[] cajeros = new ATM[5];
        for (int i = 0; i < 5; i++) {
            // Creamos un nuevo cajero (ATM) con un nombre único y el semáforo compartido
            cajeros[i] = new ATM("Cajero " + (i + 1), semaphore);

            // Iniciamos el hilo del cajero para que comience a funcionar
            cajeros[i].start();
        }
    }
}


