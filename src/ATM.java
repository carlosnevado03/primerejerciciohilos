import java.util.concurrent.Semaphore;

public class ATM extends Thread {
    private String name;
    private Semaphore semaphore;

    //Constructor con el string y el semaforo
    public ATM(String name, Semaphore semaphore) {
        this.name = name;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            // Esperar a adquirir un permiso del semáforo
            semaphore.acquire();

            // Generar un tiempo aleatorio entre 3 y 10 segundos
            int tiempo = (int)(Math.random() * 8) + 3; // Tiempo aleatorio entre 3 y 10 segundos

            // Indicar que el ATM está en uso
            System.out.println("ATM " + name + " está en uso.");

            // Simular el tiempo de uso del ATM
            Thread.sleep(tiempo * 1000); // Convertir segundos a milisegundos

            // Indicar que el ATM ha terminado de ser utilizado
            System.out.println("ATM " + name + " terminó de ser utilizado.");
        } catch (InterruptedException e) {
            // Capturar cualquier interrupción del hilo y manejarla
            System.out.println("Error");
        } finally {
            // Liberar el permiso del semáforo después de su uso, garantizando que se libere
            semaphore.release();
        }
    }

}

