import java.util.concurrent.Semaphore;

public class Supermercado {
    public static void main(String[] args) {
        // Creamos un semáforo con 2 permisos disponibles (máximo 2 clientes simultáneos)
        Semaphore semaphore = new Semaphore(2);

        // Creamos 3 cajas con distintos números de productos
        Caja caja1 = new Caja("Caja 1", 5, semaphore);
        Caja caja2 = new Caja("Caja 2", 3, semaphore);
        Caja caja3 = new Caja("Caja 3", 7, semaphore);

        // Creamos hilos para las cajas
        Thread hiloCaja1 = new Thread(caja1);
        Thread hiloCaja2 = new Thread(caja2);
        Thread hiloCaja3 = new Thread(caja3);

        // Iniciamos los hilos
        hiloCaja1.start();
        hiloCaja2.start();
        hiloCaja3.start();
    }
}
